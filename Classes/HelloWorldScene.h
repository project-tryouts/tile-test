#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);


	cocos2d::Size mTileSize = cocos2d::Size (64, 32);
	cocos2d::Size mMapSize = cocos2d::Size (10, 10);
	cocos2d::Size mMapPixelSize = cocos2d::Size::ZERO;

	cocos2d::DrawNode* m_pDebugDrawTouch = nullptr;
	bool onTouchBegan (cocos2d::Touch* pTouch, cocos2d::Event* pEvent);


	cocos2d::DrawNode* m_pDebugDrawMap = nullptr;
	void DrawStaggeredGrid (cocos2d::DrawNode* pDrawNode, const cocos2d::Color4F& lineColor, const cocos2d::Size& tileSize, const cocos2d::Size& mapSize);
	void DrawStaggeredGridPosition (cocos2d::Node* pNode, const cocos2d::Size& tileSize, const cocos2d::Size& mapSize, std::string fontName, float fontSize);
	cocos2d::Vec2 getPixelPositionForStaggeredAt (const cocos2d::Vec2& tilePos, const cocos2d::Size& mapSize, const cocos2d::Size& tileSize);
	cocos2d::Vec2 getTilePositionForStaggeredAt (const cocos2d::Vec2& pixelPos, const cocos2d::Size& mapSize, const cocos2d::Size& tileSize);

	void DrawRectangles (cocos2d::DrawNode* pDrawNode, const cocos2d::Color4F& lineColor, const cocos2d::Size& tileSize, const cocos2d::Size& mapSize);
};

#endif // __HELLOWORLD_SCENE_H__
