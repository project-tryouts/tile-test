#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));

    if (closeItem == nullptr ||
        closeItem->getContentSize().width <= 0 ||
        closeItem->getContentSize().height <= 0)
    {
        problemLoading("'CloseNormal.png' and 'CloseSelected.png'");
    }
    else
    {
        float x = origin.x + visibleSize.width - closeItem->getContentSize().width/2;
        float y = origin.y + closeItem->getContentSize().height/2;
        closeItem->setPosition(Vec2(x,y));
    }

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label

    auto label = Label::createWithTTF("Hello World", "fonts/Marker Felt.ttf", 24);
    if (label == nullptr)
    {
        problemLoading("'fonts/Marker Felt.ttf'");
    }
    else
    {
        // position the label on the center of the screen
        label->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height - label->getContentSize().height));

        // add the label as a child to this layer
        this->addChild(label, 1);
    }

    // add "HelloWorld" splash screen"
    auto sprite = Sprite::create("HelloWorld.png");
    if (sprite == nullptr)
    {
        problemLoading("'HelloWorld.png'");
    }
    else
    {
        // position the sprite on the center of the screen
        sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

        // add the sprite as a child to this layer
        this->addChild(sprite, 0);
    }


	mMapPixelSize.width = mMapSize.width * mTileSize.width;
	mMapPixelSize.height = mMapSize.height * mTileSize.height / 2;

	///////////////////////////////////////////////////////////////////////
	cocos2d::Color4F lineColor (cocos2d::Color4F::RED);
	m_pDebugDrawMap = cocos2d::DrawNode::create ();
	m_pDebugDrawMap->setContentSize (cocos2d::Size(mMapSize.width * mTileSize.width, mMapSize.height * mTileSize.height / 2));
	addChild (m_pDebugDrawMap);

	// render the grid and axis aligned bounding box(aabb)
	DrawStaggeredGrid (m_pDebugDrawMap, lineColor, mTileSize, mMapSize);

	// draw the grid tile position at the pixel position
	DrawStaggeredGridPosition (m_pDebugDrawMap, mTileSize, mMapSize, "arial", 16.f);


	// position the draw in the center
	const cocos2d::Size& winSize = cocos2d::Director::getInstance ()->getWinSize ();
	m_pDebugDrawMap->setPosition ((winSize - m_pDebugDrawMap->getContentSize ())* 0.5f);

	///////////////////////////////////////////////////////////////////////

	cocos2d::EventListenerTouchOneByOne* pTouchListener = cocos2d::EventListenerTouchOneByOne::create ();
	pTouchListener->onTouchBegan = CC_CALLBACK_2 (HelloWorld::onTouchBegan, this);
	
	// debug
	//=============================================================================================================================================================================//
	//DrawRectangles (m_pDebugDrawMap, lineColor, mTileSize, mMapSize);
	//cocos2d::DrawNode* dn = cocos2d::DrawNode::create ();
	//dn->drawLine (origin, cocos2d::Vec2 (origin.x, origin.y + mTileSize.height), cocos2d::Color4F::YELLOW);
	//m_pDebugDrawMap->addChild (dn, 10000);
	//=============================================================================================================================================================================//

	cocos2d::EventDispatcher* pEventDispatcher = getEventDispatcher ();
	pEventDispatcher->addEventListenerWithSceneGraphPriority (pTouchListener, this);
	///////////////////////////////////////////////////////////////////////
    return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);


}





bool HelloWorld::onTouchBegan (cocos2d::Touch* pTouch, cocos2d::Event* pEvent)
{
	CCASSERT (m_pDebugDrawMap, "null debug draw node");

	cocos2d::Vec2 touchPos = m_pDebugDrawMap->convertTouchToNodeSpace (pTouch);
	CCLOG ("debug draw touchPos: {%0.2f, %0.2f}", touchPos.x, touchPos.y);

	if (!m_pDebugDrawTouch && m_pDebugDrawMap)
	{
		m_pDebugDrawTouch = cocos2d::DrawNode::create ();
		m_pDebugDrawTouch->setContentSize (m_pDebugDrawMap->getContentSize ());
		addChild (m_pDebugDrawTouch);

		// position the draw in the center
		const cocos2d::Size& winSize = cocos2d::Director::getInstance ()->getWinSize ();
		m_pDebugDrawTouch->setPosition ((winSize - m_pDebugDrawTouch->getContentSize ())* 0.5f);
	}

	m_pDebugDrawTouch->clear ();
	m_pDebugDrawTouch->drawDot (touchPos, 2.f, cocos2d::Color4F::GREEN);


	///////////////////////////////////////////////////////////////////////
	// get the tile position based on the touch position
	cocos2d::Vec2 tilePos = getTilePositionForStaggeredAt (touchPos, mMapSize, mTileSize);
	CCLOG ("debug draw tilePos: {%0.2f, %0.2f}", tilePos.x, tilePos.y);

	///////////////////////////////////////////////////////////////////////

	return false;
}



cocos2d::Vec2 HelloWorld::getPixelPositionForStaggeredAt (const cocos2d::Vec2& tilePos, const cocos2d::Size& mapSize, const cocos2d::Size& tileSize)
{
	float diffX = 0;
	if ((int)tilePos.y % 2 == 1)
	{
		diffX = tileSize.width / 2;
	}
	return cocos2d::Vec2 (
		tilePos.x * tileSize.width + diffX,
		(mapSize.height - tilePos.y - 1) * tileSize.height / 2);
} // HelloWorld::getPixelPositionForStaggeredAt

cocos2d::Vec2 HelloWorld::getTilePositionForStaggeredAt (const cocos2d::Vec2& pixelPos, const cocos2d::Size& mapSize, const cocos2d::Size& tileSize)
{
	// reference:
	// https://gamedev.stackexchange.com/a/48507
	cocos2d::Vec2 tilePos1 (pixelPos.x / tileSize.width, fabs (mMapPixelSize.height - pixelPos.y) / tileSize.height);
	cocos2d::Vec2 tilePos2 (pixelPos.x / tileSize.width, fabs (mMapPixelSize.height - pixelPos.y) / tileSize.height);

	// need to reverse the y-axis
	float mapPixelHeight = mapSize.height * tileSize.height * 0.5f;
	float yPos = fabs (mapPixelHeight - pixelPos.y);

	

	//// BETTER
	//// reference:
	//// https://2dengine.com/?p=isometric
	//// origin: bottom left
	//// first tile: {1, 1}
	//float ty = pixelPos.y - pixelPos.x / 2 - tileSize.height;
	//float tx = pixelPos.x + ty;
	//ty = ceil (-ty / (tileSize.width / 2));
	//tx = ceil (tx / (tileSize.width / 2)) + 1;
	//float x = floor ((tx + ty) / 2);
	//float y = ty - tx;

	//return cocos2d::Vec2  (x, y);


	// NOT ACCURATE
	// reference:
	// https://stackoverflow.com/a/50302564/2531281
	cocos2d::Vec2 tilePos001;
	{
		float modHeight = mMapSize.height / 2 - 0.5f;

		int px = pixelPos.x;
		int py = pixelPos.y * -1 + modHeight * mTileSize.height;
		int w = tileSize.width;
		int h = tileSize.height;

		// find the coord that the pixel is in, in terms of the small rectangles
		int x = (float)(2 * px) / w;
		int y = (float)(2 * py) / h;
		//if (0 > px)
		//{
		//	x -= 1;
		//}
		//if (0 > py)
		//{
		//	y -= 1;
		//}

		// relative x and y value in small rectangle
		int px_ = px % (w / 2);
		int py_ = py % (h / 2);
		// if pixel is in A or D, reverse px
		if (0 == (x + y) % 2)
		{
			px_ = w / 2 - px_;
		}
		// check if point is pass tangent
		bool isPassTangent = (abs (py_) > ((float)px_ * (float)h / w));

		// amount to modify final coordinate by
		int x_ = 0;// isPassTangent * (0 == y % 2 ? -1 : 1);
		int y_ = 0;// isPassTangent;
		
		if (0 == x % 2)
		{
			if (isPassTangent)
			{
				y_ = 1;
			}
		}
		else
		{
			if (isPassTangent)
			{
				if (0 == y % 2)
				{
					x_ = -1;
					y_ = 1;
				}
				else
				{
					x_ = 1; 
					y_ = 1;
				}
			}
		}

		if (1 == y % 2)
		{
			x -= 1;// (0 <= x ? 1 : -1);
		}
		x = (x + 1) / 2;

		tilePos001.x = x + x_;
		tilePos001.y = y + y_;
		CCLOG ("tilePos001: {%0.2f, %0.2f}", tilePos001.x, tilePos001.y);
	}
	return tilePos001;

	cocos2d::Vec2 newTilePos;

	// NOT ACCURATE
	// reference:
	// https://stackoverflow.com/a/50274919/2531281
	{
		float x = round (pixelPos.x / tileSize.width);
		float y = 2 * round (yPos / tileSize.height);
		float dx = pixelPos.x / tileSize.width - x;
		float dy = yPos / tileSize.height - y / 2;
		if (dx + dy > 0.5)
			y = y + 1;
		else if (dx - dy > 0.5)
			y = y - 1;
		else if (-dx + dy > 0.5)
		{
			x = x - 1;
			y = y + 1;
		}
		else if (-dx - dy > 0.5)
		{
			x = x - 1;
			y = y - 1;
		}
		newTilePos = cocos2d::Vec2 (x, y);
	}
	return newTilePos;
} // HelloWorld::getTilePositionForStaggeredAt

void HelloWorld::DrawStaggeredGrid (cocos2d::DrawNode* pDrawNode, const cocos2d::Color4F& lineColor, const cocos2d::Size& tileSize, const cocos2d::Size& mapSize)
{
	CCASSERT (pDrawNode, "null draw node parameter");

	float halfTileWidth = tileSize.width / 2;
	float halfTileHeight = tileSize.height / 2;

	float xMinPos = -1 * tileSize.width / 2;
	float xMaxPos = mapSize.width * tileSize.width;
	float yMinPos = -1 * tileSize.height / 2;
	float yMaxPos = mapSize.height * tileSize.height / 2;
	///////////////////////////////////////////////////////////////////////
	// draw the boundary
	pDrawNode->drawLine (
		cocos2d::Vec2 (xMinPos, yMaxPos),
		cocos2d::Vec2 (xMaxPos, yMaxPos),
		cocos2d::Color4F::ORANGE);
	pDrawNode->drawLine (
		cocos2d::Vec2 (xMaxPos, yMaxPos),
		cocos2d::Vec2 (xMaxPos, yMinPos),
		cocos2d::Color4F::ORANGE);
	pDrawNode->drawLine (
		cocos2d::Vec2 (xMaxPos, yMinPos),
		cocos2d::Vec2 (xMinPos, yMinPos),
		cocos2d::Color4F::ORANGE);
	pDrawNode->drawLine (
		cocos2d::Vec2 (xMinPos, yMinPos),
		cocos2d::Vec2 (xMinPos, yMaxPos),
		cocos2d::Color4F::ORANGE);
	///////////////////////////////////////////////////////////////////////



	// draw \ lines //
	// draw from bottom left to the middle
	cocos2d::Vec2 pos (xMinPos, yMinPos);
	for (int i = 0; i <= mapSize.width; i++)
	{
		cocos2d::Vec2 startPos (pos); // top side
		cocos2d::Vec2 endPos (pos); // left side
		startPos.y += i * tileSize.height;
		endPos.x += i * tileSize.width;

		pDrawNode->drawLine (startPos, endPos, lineColor);
	}
	// draw from top right to the middle
	pos = cocos2d::Vec2 (xMaxPos, yMaxPos);
	for (int i = 0; i <= mapSize.width; i++)
	{
		cocos2d::Vec2 startPos (pos); // right side
		cocos2d::Vec2 endPos (pos); // bottom side
		startPos.y -= i * tileSize.height;
		endPos.x -= i * tileSize.width;

		pDrawNode->drawLine (startPos, endPos, lineColor);
	}


	// draw / lines //
	// draw from top left to the middle
	pos = cocos2d::Vec2 (xMinPos, yMaxPos);
	for (int i = 0; i <= mapSize.width; i++)
	{
		cocos2d::Vec2 startPos (pos + cocos2d::Vec2 (halfTileWidth, 0)); // top side
		cocos2d::Vec2 endPos (pos + cocos2d::Vec2 (0, -halfTileHeight)); // left side
		startPos.x += i * tileSize.width;
		endPos.y -= i * tileSize.height;

		pDrawNode->drawLine (startPos, endPos, lineColor);
	}
	pos = cocos2d::Vec2 (xMaxPos, yMinPos);
	for (int i = 0; i <= mapSize.width; i++)
	{
		cocos2d::Vec2 startPos (pos + cocos2d::Vec2 (-halfTileWidth, 0)); // bottom side
		cocos2d::Vec2 endPos (pos + cocos2d::Vec2 (0, halfTileHeight)); // right side
		startPos.x -= i * tileSize.width;
		endPos.y += i * tileSize.height;

		pDrawNode->drawLine (startPos, endPos, lineColor);
	}
} // HelloWorld::DrawGrid

void HelloWorld::DrawStaggeredGridPosition (cocos2d::Node* pNode, const cocos2d::Size& tileSize, const cocos2d::Size& mapSize, std::string fontName, float fontSize)
{
	CCASSERT (pNode, "null node parameter");

	for (int y = 0; y < mapSize.height; y++) {
		for (int x = 0; x < mapSize.width; x++) {
			cocos2d::Vec2 tilePos (x, y);
			cocos2d::Vec2 tilePixelPos = HelloWorld::getPixelPositionForStaggeredAt (tilePos, mapSize, tileSize);

			///////////////////////////////////////////////////////////////////////////
			// create the label and set at the position
			///////////////////////////////////////////////////////////////////////////
			std::ostringstream oss;
			oss << "{" << tilePos.x << "," << tilePos.y << "}";
			std::string txt = oss.str ();
			cocos2d::Label* pLabel = cocos2d::Label::createWithSystemFont (txt, fontName, fontSize);
			pLabel->setPosition (tilePixelPos);
			pNode->addChild (pLabel);
			///////////////////////////////////////////////////////////////////////////
		} // loop x-axis
	} // loop y-axis
} // HelloWorld::DrawStaggeredGridPosition

void HelloWorld::DrawRectangles (cocos2d::DrawNode* pDrawNode, const cocos2d::Color4F& lineColor, const cocos2d::Size& tileSize, const cocos2d::Size& mapSize)
{
	CCASSERT (pDrawNode, "null node parameter");

	float halfTileWidth = tileSize.width / 2;
	float halfTileHeight = tileSize.height / 2;

	float xMinPos = -mapSize.width * tileSize.width;
	float xMaxPos = mapSize.width * tileSize.width;
	float yMinPos = -1 * tileSize.height / 2;
	float yMaxPos = mapSize.height * tileSize.height;// / 2;

	// draw the mini rectangles
	for (int x = -mapSize.width; x < mapSize.width * 2; x++)
	{
		float x_ = halfTileWidth * x;
		pDrawNode->drawLine (
			cocos2d::Vec2 (x_, yMinPos),
			cocos2d::Vec2 (x_, yMaxPos),
			cocos2d::Color4F (0.0f, 1.0f, 1.0f, 1.0f));
	}
	for (int y = 0; y < mapSize.height * 2; y++) 
	{
		float y_ = halfTileHeight * y;
		pDrawNode->drawLine (
			cocos2d::Vec2 (xMinPos, y_),
			cocos2d::Vec2 (xMaxPos, y_),
			cocos2d::Color4F (0.0f, 1.0f, 1.0f, 1.0f));
	}
}